update_crypto_policies is a simple puppet module to run
update-crypto-policies --set policy

update_crypto_policies::policy:
    LEGACY
    DEFAULT
    FUTURE

The command was introduced in CentOS 8
