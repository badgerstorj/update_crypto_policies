#policy can be LEGACY, DEFAULT or FUTURE
class update_crypto_policies (String $policy = 'FUTURE') {
    exec { "update-crypto-policies --set ${policy}":
        path   => '/usr/bin',
        unless => "grep ${policy} /etc/crypto-policies/config",
        onlyif => 'test -x /usr/bin/update-crypto-policies'
    }
}
